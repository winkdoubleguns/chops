# chops
CHess OPeningS -- starts a game with a random chess opening for each side then allows you to play from that point


https://github.com/cdrchops/chessboardjs -- visual side

https://github.com/cdrchops/chess.js -- chess logic

stockfish for server side analytics -- https://github.com/cdrchops/stockfish.js?

ftp://ftp.cs.kent.ac.uk/pub/djb/pgn-extract/eco.pgn for ECO openings in PGN format



https://github.com/cdrchops/chess.js
http://chessboardjs.com/examples#1004
https://github.com/cdrchops/chops
https://www.google.com/search?q=uci+notation+stockfish&rlz=1C5CHFA_enUS721US722&oq=uci+notation+stockfish&aqs=chrome..69i57.4285j0j7&sourceid=chrome&ie=UTF-8#q=uci+notation+stockfish+javascript
https://en.wikipedia.org/wiki/Chess_engine#Interface_protocol
https://chessprogramming.wikispaces.com/Stockfish
https://www.google.com/search?rlz=1C5CHFA_enUS721US722&biw=1920&bih=956&q=uci+chess+commands&sa=X&ved=0ahUKEwin6du5r5TVAhVJ7IMKHfPABLMQ1QIIYCgA
https://www.google.com/search?rlz=1C5CHFA_enUS721US722&biw=1920&bih=956&q=stockfish+command+line+tutorial&sa=X&ved=0ahUKEwin6du5r5TVAhVJ7IMKHfPABLMQ1QIIYSgB
https://medium.freecodecamp.org/simple-chess-ai-step-by-step-1d55a9266977
http://op12no2.me/toys/lozza/index.htm


todo: fix parser so that single quotes are escaped instead of removed

add output writer to make the parsed.js file automatically instead of the parsed.eco.json file

eco.pgn2.txt is a shortened list of ECO entries designed to allow testing and parsing of ECOs and variants text to determine if it's compatible (such as embedded single quotes)

remove console logging

remove todos