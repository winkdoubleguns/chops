def f = new File("/Users/standtrooper/projects/chops/eco.pgn.txt").readLines()
def f2 = new File("/Users/standtrooper/projects/chops/parsed.eco.json")

class ECO {
    String name;
    String ecoNumber;
    String variation = "";
    String pgn;
}

def lst = []
def eco = new ECO();

f.eachWithIndex {it, idx ->
    if (it.startsWith("[ECO")) {
        eco = new ECO();
        eco.ecoNumber = it.substring(0, it.indexOf("]"))
    } else if (it.startsWith("[Opening")) {
        eco.name = it.substring(10, it.size() - 2).replaceAll("'", "")
    } else if (it.startsWith("[Variation ")) {
        eco.variation = it.substring(12, it.size() - 2).replaceAll("'", "")
    } else if (it.startsWith("1. ")) {
        def tmp = it;
        if (f[idx+1] != '') {
            tmp += " ${f[idx+1]}"
        }

        eco.pgn = tmp;
        lst << eco
    }


}

lst.each {

    f2 << "{"
    f2 << "'ecoNumber': '${it.ecoNumber}',"
    f2 << "'name' : '${it.name}',"
    f2 << "'variation' : '${it.variation}',"
    f2 << "'pgn' : '${it.pgn}'"
    f2 << "},\n"
}